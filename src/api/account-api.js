import api from '@/api/index'
import { axios } from '@/utils/request'

export function searchAccount (params) {
  return axios({
    url: api.searchAccount,
    method: 'get',
    params: params
  })
}
export function createAccount (params) {
  return axios({
    url: api.createAccount,
    method: 'POST',
    data: params
  })
}
export function updateAccount (params) {
  return axios({
    url: api.updateAccount + '/' + params.account.accountNumber,
    method: 'PUT',
    data: params
  })
}
export function deleteAccount (params) {
  return axios({
    url: api.deleteAccount + '/' + String(params.id),
    method: 'DELETE'
  })
}
export function getAccount (params) {
  return axios({
    url: api.deleteAccount + '/' + String(params.account_number),
    method: 'GET'
  })
}
