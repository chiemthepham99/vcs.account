const api = {
  Login: '/login',
  Logout: '/logout',
  ForgePassword: '/auth/forge-password',
  Register: '/auth/register',
  twoStepCode: '/auth/2step-code',
  SendSms: '/account/sms',
  SendSmsErr: '/account/sms_err',
  // get my info
  UserInfo: '/api/getAllUserData',
  // COMMON
  GlobalList: '/global-list/findByCode',
  GlobalListByProduct: '/global-list/findByCodeAndProduct',
  // Global List
  GlobalListItems: '/global-list/',
  GlobalListItemCreate: '/global-list/create',
  GlobalListItemUpdate: '/global-list/update',
  GlobalListItemDelete: '/global-list/delete/:id',
  // Global List Value
  GlobalListValueCreate: '/global-list-values/create',
  GlobalListValueUpdate: '/global-list-values/update',
  GlobalListValueDelete: '/global-list-values/delete/:id',
  // Area
  ListProvince: '/area/getProvinceList',
  ListDistrict: '/area/getDistrictListByProvince',
  ListPhoenix: '/area/getPrecinctListByDistrict',

  // GlobalParam
  ProfileItems: '/global-param/findByCode',
  ProfileItemCreate: '/global-param/createGlobalParam',
  ProfileItemUpdate: '/global-param/updateGlobalParam',
  ProfileItemChangeStatus: '/global-param/changeStatus',
  // searchDocumentRequired

  // Quản lý tài khoản
  searchAccount: 'accounts',
  createAccount: 'accounts',
  updateAccount: '/account',
  deleteAccount: '/account'

}
export default api
