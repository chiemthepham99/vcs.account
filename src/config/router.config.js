
export const asyncRouterMap = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../pages/Login.vue'),
    meta: { authRequired: false }

  },
  {
    path: '/',
    name: 'dashboard',
    component: () => import('../pages/dashboard'),
    meta: { authRequired: true }
    // redirect: { name: 'survey_flow' }
  },
  {
    path: '/account',
    name: 'account',
    component: () => import('../pages/Account/Index'),
    meta: { authRequired: true }

  },
  {
    path: '/account/create',
    name: 'account.create',
    component: () => import('../pages/Account/_child/Create.vue'),
    meta: { authRequired: true }
  },
  {
    path: '/account/:id/updateAccount',
    name: 'account.update',
    component: () => import('../pages/Account/_child/Update.vue'),
    meta: { authRequired: true }
  }
]

export const constantRouterMap = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../pages/Login.vue'),
    meta: { authRequired: true }

  }

]
