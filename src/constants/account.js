const FILTER_ALL = [
  {
    value: '',
    name: '-- All --'
  }
]
const ACCOUNT_STATUS_SEX = [
  {
    value: 'f',
    name: 'Female'
  },
  {
    value: 'm',
    name: 'Male'
  }
]
const ACCOUNT_STATUS = [
  {
    value: '0',
    name: 'New'
  },
  {
    value: '1',
    name: 'Active'
  },
  {
    value: '2',
    name: 'Pending'
  },
  {
    value: '3',
    name: 'Close'
  },
  {
    value: '4',
    name: 'Remove'
  }
]
const ACCOUNT_STATUS_ALL = FILTER_ALL.concat(ACCOUNT_STATUS)
const TYPE_COMPONENT = [
  {
    value: 'editInline',
    name: 'Editing on the table'
  },
  {
    value: 'editOnPage',
    name: 'Editing on page'
  }
]
const CUSTOMER_TYPE = [
  {
    value: '0',
    name: 'Individual '
  },
  {
    value: '1',
    name: 'Business'
  }
]
export { ACCOUNT_STATUS_SEX, ACCOUNT_STATUS, TYPE_COMPONENT, CUSTOMER_TYPE, ACCOUNT_STATUS_ALL }
