const PROVINCE = [
  {
    'state_code': 'Hà Nội',
    'state_name': 'Hà Nội'
  },
  {
    'state_code': 'Hồ Chí Minh',
    'state_name': 'Hồ Chí Minh'
  }
]
const DISTRICT = [
  {
    'state_code_father': 'Hà Nội',
    'state_name': 'Phúc Thọ',
    'state_code': 'Phúc Thọ'
  },
  {
    'state_code_father': 'Hà Nội',
    'state_name': 'Đan Phượng',
    'state_code': 'Đan Phượng'
  },
  {
    'state_code_father': 'Hà Nội',
    'state_name': 'Cầu Giấy',
    'state_code': 'Cầu Giấy'
  },
  {
    'state_code_father': 'Hồ Chí Minh',
    'state_name': 'Quận 1',
    'state_code': 'Quận 1'
  },
  {
    'state_code_father': 'Hồ Chí Minh',
    'state_name': 'Quận 2',
    'state_code': 'Quận 2'
  }
]
export { PROVINCE, DISTRICT }
