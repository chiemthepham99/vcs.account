import '@babel/polyfill'
import Vue from 'vue'
import Antd, { FormModel } from 'ant-design-vue'
import router from './router'
import i18n from './i18n'
import store from './store/store'
import App from './App'
import { VueAxios } from './utils/request'
import 'ant-design-vue/dist/antd.less'
import PageLoading from './components/PageLoading'
import Storage from 'vue-ls'
import VueDraggableResizable from 'vue-draggable-resizable'
import VueJWT from 'vuejs-jwt'
// optionally import default styles
import 'vue-draggable-resizable/dist/VueDraggableResizable.css'
import VueCurrencyInput from 'vue-currency-input'

const pluginOptions = {
  globalOptions: { currency: 'VND' }
}
Vue.use(VueCurrencyInput, pluginOptions)
Vue.component('vue-draggable-resizable', VueDraggableResizable)
require('./utils/mixins')
require('./utils/filters')
Vue.config.productionTip = false

const options = {
  namespace: 'vuejs__', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local' // storage name session, local, memory
}
Vue.use(PageLoading)
Vue.use(VueJWT, options)
Vue.use(Storage, options)

Vue.use(Antd)
Vue.use(FormModel)
// mount axios Vue.$http and this.$http
Vue.use(VueAxios)

new Vue({
  i18n,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
