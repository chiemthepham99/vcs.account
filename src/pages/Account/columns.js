const VARIABLENAME = {
  accountNumber: 'accountNumber', // Số tài khoản
  accountType: 'accountType', // Loại tài khoản
  accountName: 'accountName', // Tên tài khoản
  customerEstablished: 'customerPdDob', // Ngày thành lập
  accountBalance: 'accountBalance', // Số dư tài khoản
  customerProvince: 'customerProvince', // Tỉnh
  customerDistrict: 'customerDistrict', // Huyện
  customerEmail: 'customerEmail', // Email
  customerGender: 'customerGender', // Giới tính
  accountStatus: 'accountStatus' // Trạng thái tài khoản
}

const columns = [
  {
    slots: { title: 'actionTitle' },
    dataIndex: 'operation',
    scopedSlots: { customRender: 'operation' },
    width: 100,
    align: 'center'
  },
  {
    title: 'Account No.',
    dataIndex: VARIABLENAME.accountNumber,
    scopedSlots: { customRender: VARIABLENAME.accountNumber },
    align: 'right',
    ellipsis: true,
    width: 120
  },
  {
    title: 'Account Type',
    dataIndex: VARIABLENAME.accountType,
    scopedSlots: { customRender: VARIABLENAME.accountType },
    align: 'left',
    ellipsis: true,
    width: 120
  },
  {
    title: 'Account Name',
    dataIndex: VARIABLENAME.accountName,
    scopedSlots: { customRender: VARIABLENAME.accountName },
    align: 'left',
    width: 300,
    ellipsis: true
  },
  {
    title: 'D.O.E/ D.O.B',
    dataIndex: VARIABLENAME.customerEstablished,
    scopedSlots: { customRender: VARIABLENAME.customerEstablished },
    align: 'center',
    width: 150,
    ellipsis: true
  },
  {
    title: 'Gender',
    dataIndex: VARIABLENAME.customerGender,
    scopedSlots: { customRender: VARIABLENAME.customerGender },
    align: 'left',
    width: 100,
    ellipsis: true
  },
  {
    title: 'Email',
    dataIndex: VARIABLENAME.customerEmail,
    scopedSlots: { customRender: VARIABLENAME.customerEmail },
    align: 'left',
    width: 200,
    ellipsis: true
  },
  {
    title: 'Balance',
    dataIndex: VARIABLENAME.accountBalance,
    scopedSlots: { customRender: VARIABLENAME.accountBalance },
    align: 'right',
    width: 120,
    ellipsis: true
  },
  {
    title: 'Status',
    dataIndex: VARIABLENAME.accountStatus,
    scopedSlots: { customRender: VARIABLENAME.accountStatus },
    align: 'left',
    ellipsis: true,
    width: 150
  },
  {
    title: 'The Provincial',
    dataIndex: VARIABLENAME.customerProvince,
    scopedSlots: { customRender: VARIABLENAME.customerProvince },
    align: 'left',
    width: 150,
    ellipsis: true
  },
  {
    title: 'District',
    dataIndex: VARIABLENAME.customerDistrict,
    scopedSlots: { customRender: VARIABLENAME.customerDistrict },
    align: 'left',
    width: 150,
    ellipsis: true
  }
]
export { VARIABLENAME, columns }
