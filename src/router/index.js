import Vue from 'vue'
import VueRouter from 'vue-router'
import VueMeta from 'vue-meta'
import NProgress from 'nprogress/nprogress'
import '@/styles/nprogress.less' // progress bar custom style

import store from '../store/store'
import { asyncRouterMap } from '../config/router.config'

NProgress.configure({ showSpinner: false }) // NProgress Configuration
// hack router push callback
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(VueRouter)
Vue.use(VueMeta, {
  keyName: 'page'
})

const router = new VueRouter({
  routes: asyncRouterMap,
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

// Before each route evaluates...
router.beforeEach((routeTo, routeFrom, next) => {
  // If this isn't an initial page load...
  if (routeTo.name !== null) {
    NProgress.start()
  }
  if (routeTo.name !== 'account' && routeTo.name !== 'account.create' && routeTo.name !== 'account.update') {
    const filter = window.localStorage.getItem('filter.account')
    if (filter) {
      window.localStorage.setItem('filter.account', null)
    }
  }
  function redirectToLogin () {
    return next({ name: 'login', query: { redirectFrom: routeTo.fullPath } })
  }
  const authRequired = routeTo.matched.some((route) => route.meta.authRequired)
  // If auth is required and the user is logged in...
  if (store.getters['auth/loggedIn']) {
    if (routeTo.name === 'login') {
      return next({ name: 'account' })
    } else {
      return next()
    }
  } else {
    if (!authRequired && routeTo.name != null) {
      return next()
    } else {
      return redirectToLogin()
    }
  }
})

router.beforeResolve(async (routeTo, routeFrom, next) => {
  try {
    for (const route of routeTo.matched) {
      await new Promise((resolve, reject) => {
        if (route.meta && route.meta.beforeResolve) {
          route.meta.beforeResolve(routeTo, routeFrom, (...args) => {
            if (args.length) {
              if (routeFrom.name === args[0].name) {

              }
              next(...args)
              reject(new Error('Redirected'))
            } else {
              resolve()
            }
          })
        } else {
          resolve()
        }
      })
    }
  } catch (error) {
    return
  }
  next()
})

router.afterEach((routeTo, routeFrom) => {
  NProgress.done()
})

export default router
