import { mapState, mapGetters, mapActions } from 'vuex'
import {
} from '@/constants/global_list'
// import _concat from 'lodash/concat'

export const authComputed = {
  ...mapState('auth', {
    currentUser: (state) => state.currentUser
  }),
  ...mapGetters('auth', ['loggedIn'])
}
export const globalListComputed = {
  ...mapState('common', {

  })
}
export const authMethods = mapActions('auth', ['logIn', 'logOut'])
export const commonMethods = mapActions('common', ['fetchProvinces', 'fetchDistricts', 'fetchPhoenixes', 'fetchGlobalList'])
export const commonGlobalMethods = mapActions('common', ['fetchGlobalList'])
