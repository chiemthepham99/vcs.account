import { login, getInfo } from '@/api/login'
import Vue from 'vue'

export const state = {
  currentUser: getSavedState('auth.currentUser'),
  roles: [],
  permissions: [],
  info: {}
}

export const mutations = {
  SET_CURRENT_USER (state, newValue) {
    state.currentUser = newValue
    saveState('auth.currentUser', newValue)
    setDefaultAuthHeaders(state)
  },
  SET_PERMISSIONS (state, permissions) {
    state.permissions = permissions
    saveState('permissions', permissions)
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_INFO: (state, info) => {
    state.info = info
  }
}

export const getters = {
  // Whether the user is currently logged in.
  loggedIn (state) {
    return !!state.currentUser
  },
  roles (state) {
    return state.roles
  },
  currentUser () {
    return state.currentUser
  },
  permissions (state) {
    return state.permissions
  }
}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init ({ state, dispatch, commit }) {
    if (state.permissions.length === 0) {
      commit('SET_PERMISSIONS', getSavedState('permissions'))
    }
    setDefaultAuthHeaders(state)
    dispatch('validate')
  },

  // Logs in the current user.
  logIn ({ commit, dispatch, getters }, { username, password } = {}) {
    if (getters.loggedIn) return dispatch('validate')
    const params = {
      username: username,
      password: password
    }
    return login(params).then((response) => {
      const user = response.data
      if (user.access_token) {
        const jwtResult = Vue.$jwt.decode(user.access_token, 'a123B456cC@!#123')
        if (jwtResult) {
          user['info'] = jwtResult.sub
        }
      }
      commit('SET_CURRENT_USER', user)
      return response
    })
  },
  GetInfo () {
    return getInfo()
  },
  // Logs out the current user.
  logOut ({ commit }) {
    commit('SET_CURRENT_USER', null)
    commit('SET_PERMISSIONS', null)
  },

  // Validates the current user's token and refreshes it
  // with new data from the API.
  validate ({ commit, state }) {
    if (!state.currentUser) return Promise.resolve(null)

    return null

    // return getInfo().then((response) => {
    //   const user = response.data
    //   commit('SET_CURRENT_USER', user)
    //   return user
    // })
    //   .catch((error) => {
    //     if (error.response && error.response.status === 401) {
    //       commit('SET_CURRENT_USER', null)
    //     }
    //     return null
    //   })
  }
}

// ===
// Private helpers
// ===

function getSavedState (key) {
  return JSON.parse(window.localStorage.getItem(key))
}

function saveState (key, state) {
  window.localStorage.setItem(key, JSON.stringify(state))
}

function setDefaultAuthHeaders (state) {

}
