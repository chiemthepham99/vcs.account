
export function validateNumber (rule, value, callback) {
  if (value) {
    const reg = new RegExp('^[0-9]+$', 'g')
    if (!reg.test(value)) {
      callback(new Error('Only allow input of natural numbers'))
    }
    callback()
  }
  callback()
}
export function validateBalance (rule, value, callback) {
  if (value) {
    const reg = new RegExp('^[0-9]+$', 'g')
    if (!reg.test(value)) {
      callback(new Error('Only allow input of natural numbers greater than 0'))
    }
    callback()
  }
  callback()
}
