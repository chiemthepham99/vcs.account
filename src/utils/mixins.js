import Vue from 'vue'
import moment from 'moment'
import { API_ERROR_STATUSES } from '@/constants'
// import _find from 'lodash/find'
import * as EmailValidator from 'email-validator'
import { removeUtf8 } from '@/utils/common'

var mixin = {
  data: function () {
    return {
      dateFormat: 'YYYY-MM-DD',
      dateSubmitFormat: 'YYYY-MM-DD',
      dateDisplayFormat: ['DD/MM/YYYY', 'DDMMYYYY'],
      datePlaceholderFormat: 'DD/MM/YYYY'
    }
  },
  methods: {

    handleApiError (err) {
      if (API_ERROR_STATUSES.indexOf(err.response.status) !== -1) {
        if (err.response.data.errorMessage) {
          return err.response.data.errorMessage
        }
      }
      return err.message
    },
    validateEmailGlobal (rule, value, cb) {
      // eslint-disable-next-line standard/no-callback-literal
      this.validateEmail(value) ? cb() : cb('error')
    },
    filterSelectOption (input, option) {
      const txt = removeUtf8(input.toLowerCase())
      const text = removeUtf8(option.componentOptions.children[0].text)
      return (
        text.toLowerCase().indexOf(txt) >= 0
      )
    },
    handleLoadedData: function (res) {
      return res.data.data
    },
    handlePaginationData: function (res) {
      if (res.pagination !== undefined) {
        return {
          total: res.pagination.total
        }
      }
      return {}
    },
    validateEmail (email) {
      if (!email) return true
      if (email.trim().length > 0) return EmailValidator.validate(email)
      return true
    },
    getTableRowIndex (pageSize, currentPage, rowIndex) {
      if (currentPage === 0) {
        currentPage = 1
      }
      return (currentPage - 1) * pageSize + rowIndex + 1
    },
    fixedColumnOfTable (table, columns) {
      const thTags = table.getElementsByTagName('th')
      if (thTags.length) {
        for (const t in thTags) {
          if (thTags.hasOwnProperty(t)) {
            const th = thTags[t]
            const key = th.getAttribute('key')
            if (key && key.length) {
              for (const col in columns) {
                if (columns.hasOwnProperty(col)) {
                  if (columns[col].dataIndex === key && columns[col].width !== undefined) {
                    if (typeof columns[col].width === 'number') {
                      th.style.width = columns[col].width + 'px'
                    } else {
                      th.style.width = columns[col].width
                    }
                    break
                  }
                }
              }
            }
          }
        }
      }
    },
    formatPrice (value) {
      const val = (value / 1).toFixed(0).replace('.', ',')
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    },
    getGlobalName (items, value) {
      if (typeof items === 'object' && items.length > 0) {
        let name = value
        items.forEach(item => {
          if (item.value === value) {
            name = item.name
          }
        })
        return name
      }
      return value
    },
    getNameByValue (list, value) {
      if (value) {
        for (let i = 0; i < list.length; i++) {
          if (list.hasOwnProperty(i) && String(list[i].value) === String(value)) {
            return list[i].name
          }
        }
      }
      return null
    },
    getDistrictNameByAreaCode (districts, code) {
      let name = ''
      districts.forEach(item => {
        if (item.areaCode === code) {
          name = item.name
        }
      })
      return name
    },
    getPhoenixNameByAreaCode (precincts, code) {
      let name = ''
      precincts.forEach(item => {
        if (item.areaCode === code) {
          name = item.name
        }
      })
      return name
    },
    convertToDisplayDate (dateString) {
      if (dateString !== null && dateString !== '') {
        const d = moment(dateString, this.dateSubmitFormat)
        if (d.isValid()) {
          return d.format(this.datePlaceholderFormat)
        }
      }
      return dateString
    },
    convertToSubmitDate (dateString) {
      if (dateString !== null && dateString !== '') {
        const d = moment(dateString, this.datePlaceholderFormat)
        if (d.isValid()) {
          return d.format(this.dateSubmitFormat)
        }
      }
      return dateString
    },
    convertPropToSubmitDate (obj) {
      if (obj === null || obj === '' || typeof (obj) !== 'object') {
        return obj
      }
      const pattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/
      for (const p in obj) {
        if (obj.hasOwnProperty(p)) {
          if ((typeof (obj[p]) === 'string' && pattern.test(obj[p])) || moment.isMoment(obj[p])) {
            obj[p] = this.convertToSubmitDate(obj[p])
          } else if (typeof (obj[p]) === 'object') {
            obj[p] = this.convertPropToSubmitDate(obj[p])
          }
        }
      }
      return obj
    },
    convertPropToDisplayDate (obj) {
      if (obj === null || obj === '' || typeof (obj) !== 'object') {
        return obj
      }
      const pattern = /^\d{4}-\d{1,2}-\d{1,2}$/
      for (const p in obj) {
        if (obj.hasOwnProperty(p)) {
          if ((typeof (obj[p]) === 'string' && pattern.test(obj[p])) || moment.isMoment(obj[p])) {
            obj[p] = this.convertToDisplayDate(obj[p])
          } else if (typeof (obj[p]) === 'object') {
            obj[p] = this.convertPropToDisplayDate(obj[p])
          }
        }
      }
      return obj
    },
    checkMenuPermission (menus, path) {
      const matchedPaths = menus.filter(function (menuItem) {
        return menuItem.objectUrl === path
      })
      return matchedPaths.length > 0
    },
    checkComponentPermission (components, code) {
      const matchedComponents = components.filter(function (componentItem) {
        return componentItem.objectCode === code
      })
      return matchedComponents.length > 0
    },
    hasMenuPermission (routeName) {
      const $this = this
      if ($this.$store.state.auth.permissions) {
        const menus = $this.$store.state.auth.permissions.menus.filter(function (menuItem) {
          if (!(menuItem.objectUrl === routeName)) {
            return $this.checkMenuPermission(menuItem.childObjects, routeName)
          }
          return true
        })
        return menus.length > 0
      }
      return false
    },
    hasUrlPermission (url) {
      return this.$store.state.auth.permissions.urls.indexOf(url) !== -1
    },
    hasComponentPermission (code) {
      const components = this.$store.state.auth.permissions.components.filter(function (componentItem) {
        if (!(componentItem.objectCode === code)) {
          return this.checkComponentPermission(componentItem.childObjects, code)
        }
        return true
      })
      return components.length > 0
    }
  }
}
Vue.mixin(mixin)
