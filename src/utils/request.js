import axios from 'axios'
import store from './../store/store'
import notification from 'ant-design-vue/es/notification'
import { VueAxios } from './axios'
import { deepTrim } from '@/utils/common'

const service = axios.create({
  baseURL: process.env.VUE_APP_API_SERVER_URL,
  timeout: 160000,
  headers: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
})

const err = (error) => {
  if (error.response) {
    const data = error.response.data
    if (error.response.status === 403) {
      notification.error({
        message: 'Forbidden',
        description: data.message
      })
    }
    if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
      store.dispatch('auth/logOut').then(() => {
        setTimeout(() => {
          // window.location.reload()
        }, 1500)
      })
    }
  }
  return Promise.reject(error)
}

// request interceptor
service.interceptors.request.use(config => {
  const currentUser = JSON.parse(window.localStorage.getItem('auth.currentUser'))
  if (config.params === undefined) {
    config['params'] = {}
  }
  if (currentUser && currentUser.access_token) {
    config.headers['Authorization'] = 'Bearer ' + currentUser.access_token
  }
  if (config.params) {
    deepTrim(config['params'])
  }
  if (config.data) {
    deepTrim(config['data'])
  }
  return config
}, err)

// response interceptor
service.interceptors.response.use((response) => {
  return response.data
}, err)

const installer = {
  vm: {},
  install (Vue) {
    Vue.use(VueAxios, service)
  }
}

export {
  installer as VueAxios,
  service as axios
}
